package br.com.trama.expandablelist;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ExpandableListView;

public class HelloAndroidActivity extends Activity {

    /**
     * Called when the activity is first created.
     * @param savedInstanceState If the activity is being re-initialized after 
     * previously being shut down then this Bundle contains the data it most 
     * recently supplied in onSaveInstanceState(Bundle). <b>Note: Otherwise it is null.</b>
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        List<Root> data = getData();
        
        ExpandableListView listView = (ExpandableListView) findViewById(R.id.listView);
        ExpandableAdapter adapter = new ExpandableAdapter(data, this.getLayoutInflater());
        listView.setAdapter(adapter);
        
        for(int i= 0; i < data.size(); i++){
        	listView.expandGroup(i);
        }
    }
    
    
    private List<Root> getData(){
    	
    	int size = 3;
    	
    	ArrayList<Root> roots = new ArrayList<Root>();
    	
    	for (int i = 0; i < size; i++) {
									
      		Root root = new Root();
    		root.setName("Root ["+i+"]");
    		
    	   	for (int z = 0; z < size; z++) {
        		root.add(new Child("Child ["+z+"]"));
    		}

    	   	roots.add(root);
 		}
    	
    	return roots;
    }
 
}

