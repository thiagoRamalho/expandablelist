package br.com.trama.expandablelist;

public class Child {

	private String name;

	public Child(String name) {
		super();
		this.name = name;
	}
	
	
	public String getName() {
		return name;
	}
}
