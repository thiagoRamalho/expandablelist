package br.com.trama.expandablelist;

import java.util.List;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class ExpandableAdapter extends BaseExpandableListAdapter {

	private List<Root> roots;
	private LayoutInflater inflater;
	
	public ExpandableAdapter(List<Root> roots, LayoutInflater inflater) {
		super();
		this.roots = roots;
		this.inflater = inflater;
	}

	public int getGroupCount() {
		return roots.size();
	}

	public int getChildrenCount(int groupPosition) {
		return roots.get(groupPosition).getChilds().size();
	}

	public Root getGroup(int groupPosition) {
		return roots.get(groupPosition);
	}

	public Child getChild(int groupPosition, int childPosition) {
		
		Root root = this.roots.get(groupPosition);
		Child child = root.getChilds().get(childPosition);
		
		return child;
	}

	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	public boolean hasStableIds() {
		return true;
	}

	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.group_layout, parent, false);
		}
		
		Root group = getGroup(groupPosition);
		
		TextView title = (TextView) convertView.findViewById(R.id.title);
		title.setText(group.getName());
		
		return convertView;
	}

	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.row_layout, parent, false);
		}
		
		Child child = getChild(groupPosition, childPosition);
		
		TextView row = (TextView) convertView.findViewById(R.id.txt_row);
		row.setText(child.getName());
		
		return convertView;
	}

	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}
