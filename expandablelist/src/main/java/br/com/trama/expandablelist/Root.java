package br.com.trama.expandablelist;

import java.util.ArrayList;
import java.util.List;

public class Root {

	private String name;
	
	private List<Child> childs = new ArrayList<Child>();
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void add(Child child){
		this.childs.add(child);
	}
	
	public List<Child> getChilds() {
		return childs;
	}
}
